let hdrActivityTextArea = document.getElementById("hdrActivityText");
let btnGetActivity = document.getElementById("btnGetActivity");

btnGetActivity.addEventListener("click", handleGetActivity);

function handleGetActivity() {
  // normal promise interaction
  fetch("https://www.boredapi.com/api/activity")
    .then((response) => response.json())
    .then((result) => (hdrActivityTextArea.innerText = result.activity));

  // async await promise interaction
  //getActivities()
}

async function getActivities() {
  // async await
  let response = await fetch("https://www.boredapi.com/api/activity");
  let result = await response.json();
  hdrActivityTextArea.innerText = result.activity;
}
